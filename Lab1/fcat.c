#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    if (argc > 2) {
        exit(0);
    }
    FILE *input=NULL, *output=NULL;
    if (argc >= 2) {
        input = fopen(argv[1], "r");
    } 
    else {
        input = stdin;
    }
    if(input == NULL) {
         exit(0);
    }
    output = stdout;
    char buffer[1024];
    while(fgets(buffer,sizeof buffer, input) != NULL) {
        fprintf(output, "%s", buffer);
    }
    fclose(input);
    return 0;
}
